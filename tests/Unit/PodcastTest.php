<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PodcastTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Create Podcast Test.
     *
     * @return void
     */
    public function testCreatePodcast()
    {
        $response = $this->json('POST', 'http://127.0.0.1:8000/api/podcast',
            [
            'name' => 'Ad-'.time(),
            "description" => "This is First Podcast",
            "marketing_url" => "http://127.0.0.1:8000/api/podcast/{1}",
            "feed_url" => "http://127.0.0.1:8000/api/podcast/{1}",
            ]
        );

        $response->assertSee(200);
    }

    /**
     * Update Podcast Test.
     *
     * @return void
     */
    public function testUpdatePodcast()
    {
        $response = $this->json('PUT', 'http://127.0.0.1:8000/api/podcast/1',
            [
                'name' => 'update1-'.time(),
                "description" => "This is First Podcast",
                "marketing_url" => "http://127.0.0.1:8000/api/podcast/{1}",
                "feed_url" => "http://127.0.0.1:8000/api/podcast/{1}",
            ]
        );

        $response->assertSee(200);
    }

    /**
     * Approve Podcast Test.
     *
     * @return void
     */
    public function testApprovalPodcast()
    {
        $response = $this->post('http://127.0.0.1:8000/api/podcast/approval/1');

        $response->assertSee(200);
    }

    /**
     * Delete Podcast Test.
     *
     * @return void
     */
    public function testDeletePodcast()
    {
        $response = $this->get('http://127.0.0.1:8000/api/podcast/1');

        $response->assertSee(200);
    }

    /**
     * Get Published Podcasts Test.
     *
     * @return void
     */
    public function testGetPublishPodcast()
    {
        $response = $this->get('http://127.0.0.1:8000/api/podcasts/published');

        $response->assertSee('data');
    }
}
