<?php

use Illuminate\Database\Seeder;

class PodcastCommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('podcast_comments')->insert([
            'podcast_id' => '2',
            'author_name' => 'Monad',
            'author_email' => 'omkar.kelkar@monadinfotech.com',
            'comment' => 'Podcast published and reading done..!',
        ]);
        DB::table('podcast_comments')->insert([
            'podcast_id' => '2',
            'author_name' => 'Monad1',
            'author_email' => 'aditya.kelkar@monadinfotech.com',
            'comment' => 'Podcast published and reading done..!',
        ]);
        DB::table('podcast_comments')->insert([
            'podcast_id' => '2',
            'author_name' => 'Monad2',
            'author_email' => 'ramesh.kelkar@monadinfotech.com',
            'comment' => 'Podcast published and reading done..!',
        ]);
        DB::table('podcast_comments')->insert([
            'podcast_id' => '3',
            'author_name' => 'Monad',
            'author_email' => 'omkar.kelkar@monadinfotech.com',
            'comment' => 'Podcast published and reading done..!',
        ]);
        DB::table('podcast_comments')->insert([
            'podcast_id' => '3',
            'author_name' => 'Monad1',
            'author_email' => 'aditya.kelkar@monadinfotech.com',
            'comment' => 'Podcast published and reading done..!',
        ]);
        DB::table('podcast_comments')->insert([
            'podcast_id' => '3',
            'author_name' => 'Monad2',
            'author_email' => 'ramesh.kelkar@monadinfotech.com',
            'comment' => 'Podcast published and reading done..!',
        ]);
    }
}
