<?php

use Illuminate\Database\Seeder;

class PodcastTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('podcasts')->insert([
            'name' => 'First',
            'description' => 'This is First Podcast',
            'marketing_url' => 'http://127.0.0.1:8000/api/podcast/{1}',
            'feed_url' => 'http://127.0.0.1:8000/api/podcast/{1}',
        ]);
        DB::table('podcasts')->insert([
            'name' => 'second',
            'description' => 'This is Second Podcast',
            'marketing_url' => 'http://127.0.0.1:8000/api/podcast/{2}',
            'feed_url' => 'http://127.0.0.1:8000/api/podcast/{2}',
        ]);
        DB::table('podcasts')->insert([
            'name' => 'Third',
            'description' => 'This is Third Podcast',
            'marketing_url' => 'http://127.0.0.1:8000/api/podcast/{3}',
            'feed_url' => 'http://127.0.0.1:8000/api/podcast/{3}',
        ]);
    }
}
