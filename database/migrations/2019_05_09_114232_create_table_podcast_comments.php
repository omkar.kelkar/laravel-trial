<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateTablePodcastComments extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('podcast_comments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('podcast_id')->unsigned()->nullable();
                $table->string('author_name')->nullable();
                $table->string('author_email')->nullable();
                $table->string('comment')->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('podcast_id')->references('id')->on('podcasts');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('podcast_comments');
        }
    }
