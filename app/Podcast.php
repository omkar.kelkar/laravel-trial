<?php

	/**
	 * This file is part of the Monad. Package Core.
	 *
	 * (c) Monad
	 *
	 */

    namespace App;

	use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Podcast extends Model
	{
	    use SoftDeletes;
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name',
        ];

        /**
         * PodcastComments - hasMany
         *
         * @return \Illuminate\Database\Eloquent\Relations\hasMany
         */

        public function comments()
        {
            return $this->hasMany(Podcast_Comment::class, 'podcast_id', 'id');
        }
	}
