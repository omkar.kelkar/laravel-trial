<?php

    /**
     * This file is part of the Monad. Package Core.
     *
     * (c) Monad
     *
     */

    namespace App;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Podcast_Comment extends Model
    {
        use SoftDeletes;
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'podcast_id',
        ];

        protected $table = [
            'podcast_comments'
        ];
    }
