<?php

	/**
	 * This file is part of the Monad. Package Core.
	 *
	 * (c) Monad
	 *
	 */

	namespace App\Http\Controllers\Api;

	use App\Podcast;
    use http\Env\Response;
    use Illuminate\Http\Request;
    use mysql_xdevapi\Collection;
    use Dingo\Api\Routing\Helpers;
    use App\Http\Controllers\Controller;
    use App\Transformers\PodcastTransformer;

    class PodcastController extends Controller
	{
        use Helpers;
		/**
		 * Get the Podcast for requested ID
		 */

		public function getPodcast($intPodcastID=NULL)
		{
            $objPodcastData = Podcast::with('comments')->where('id',$intPodcastID)->get();

            return $this->response->item($objPodcastData, new PodcastTransformer());
		}

        /**
         * postCreate Podcast method used to Create the podcast with requested data
         *
         * @param $objRequest
         *
         * @return Response error/success
         */

		public function postCreate(Request $objRequest)
		{
            //Validation on Request Data
            $validatedData = $objRequest->validate([
                'name'          => 'required|unique:podcasts|min:4',
                'description'   => 'max:1000',
                'marketing_url' => 'active_url',
                'feed_url'      => 'required|active_url',
            ]);

            //Create Object of Podcast and store new Data in object and save
            $objPodcast = new Podcast();

            //Convert Base64 image to image and store in public directory
            if ($objRequest->has('image')) {
                //Convert Base64 image to image and store in public directory
                $image = $objRequest->image;  // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = str_random(10).'.'.'png';
                \File::put(public_path(). '/' . $imageName, base64_decode($image));
                $objPodcast->image          = $image;
            }

            $objPodcast->name           = $objRequest->name;
            $objPodcast->feed_url       = $objRequest->feed_url;
            $objPodcast->description    = $objRequest->description;
            $objPodcast->marketing_url  = $objRequest->marketing_url;

            $objPodcast->save();

            return $this->response->created(null, ["message" => 'Podcast Created Successfully..!', 'status_code' => 200]);
		}

		/**
         * postUpdate Podcast method used to Update the podcast with requested data for requested ID
         *
         * @param $objRequest,$intPodcastID
         *
         * @return Response error/success
		*/

        public function PostUpdate(Request $objRequest,$intPodcastID=NULL)
        {
            //Validation on Request Data
            $validatedData = $objRequest->validate([
                'name'          => 'required|min:4|unique:podcasts,name,'.$intPodcastID,
                'description'   => 'max:1000',
                'marketing_url' => 'active_url',
                'feed_url'      => 'active_url',
            ]);

            $objPodcastData                 = Podcast::find($intPodcastID);

            if ($objPodcastData) {
                if ($objRequest->has('image')) {
                    //Convert Base64 image to image and store in public directory
                    $image = $objRequest->image;  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = str_random(10) . '.' . 'png';
                    \File::put(public_path() . '/' . $imageName, base64_decode($image));

                    $objPodcastData->image = $image;
                }
                $objPodcastData->name               = $objRequest->name;
                $objPodcastData->feed_url           = $objRequest->feed_url;
                $objPodcastData->description        = $objRequest->description;
                $objPodcastData->marketing_url      = $objRequest->marketing_url;

                $objPodcastData->update();

                return $this->response->accepted(null, ["message" => 'Podcast Updated Successfully..!', 'status_code' => 200]);
            }
            else {
                return $this->response->error('The Requested ID does not exists.','500');
            }
        }

        /**
         * getPublished Podcast method used to get the List of Published Podcasts
         *
         * @return Collection
         */

        public function getPublished()
        {
            $objPodcastData = Podcast::where('status','published')->get();

            return $this->response->collection($objPodcastData, new PodcastTransformer());
        }

        /**
         * deletePodcast Podcast method used to Delete Podcast for requested ID
         *
         * @param $intPodcastID
         *
         * @return Response error/success
         */

        public function deletePodcast($intPodcastID=NULL)
        {
            $objPodcastData = Podcast::find($intPodcastID);

            if ($objPodcastData) {

                $objPodcastData->delete();

                return $this->response->accepted(null, ["message" => 'Podcast Deleted Successfully..!', 'status_code' => 200]);
            }
            else {
                return $this->response->error('The Requested ID does not exists.','500');
            }
        }

        /**
         * postApproval Podcast method used to Update the podcast Status to published for requested ID
         *
         * @param $intPodcastID
         *
         * @return Response error/success
         */

        public function postApproval($intPodcastID=NULL)
        {
            $objPodcastData = Podcast::find($intPodcastID);

            if ($objPodcastData) {

                $objPodcastData->status = 'published';

                $objPodcastData->update();

                return $this->response->accepted(null, ["message" => 'Podcast Published Successfully..!', 'status_code' => 200]);
            }
            else {
                return $this->response->error('The Requested ID does not exists.','500');
            }
        }
	}
