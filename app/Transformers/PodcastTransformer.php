<?php

    namespace App\Transformers;

    use App\Podcast;
    use League\Fractal\TransformerAbstract;
    use App\Models\User;

    class PodcastTransformer extends TransformerAbstract
    {
        public function transform(Podcast $podcast)
        {
            return [
                'name' => $podcast->name,
                'description' => $podcast->description,
                'marketing_url' => $podcast->marketing_url,
                'feed_url' => $podcast->feed_url,
                'image' => $podcast->image,
                'status' => $podcast->status,
                'added' => date('Y-m-d', strtotime($podcast->created_at))
            ];
        }
    }
