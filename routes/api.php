<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    /**
     * Dingo Api Routes
    */
    $api = app('Dingo\Api\Routing\Router');

    $api->version('v1', [], function ($api) {

        $api->get('podcast/{podcast}',              'App\Http\Controllers\Api\PodcastController@getPodcast');
        $api->post('podcast',                       'App\Http\Controllers\Api\PodcastController@postCreate');
        $api->put('podcast/{podcast}',              'App\Http\Controllers\Api\PodcastController@PostUpdate');
        $api->get('podcasts/published',             'App\Http\Controllers\Api\PodcastController@getPublished');
        $api->delete('podcast/{podcast}',           'App\Http\Controllers\Api\PodcastController@deletePodcast');
        $api->post('podcast/approval/{podcast}',    'App\Http\Controllers\Api\PodcastController@postApproval');
    });

    /**
     * System Predefined routes
    */

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });
